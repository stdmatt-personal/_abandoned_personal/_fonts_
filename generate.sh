#!/usr/bin/env bash
##----------------------------------------------------------------------------##
##                       __      __                  __   __                  ##
##               .-----.|  |_.--|  |.--------.---.-.|  |_|  |_                ##
##               |__ --||   _|  _  ||        |  _  ||   _|   _|               ##
##               |_____||____|_____||__|__|__|___._||____|____|               ##
##                                                                            ##
##  File      : generate.sh                                                   ##
##  Project   : _fonts_                                                       ##
##  Date      : Sep 27, 2019                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2019                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##----------------------------------------------------------------------------##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source "/usr/local/src/stdmatt/shellscript_utils/main.sh";

##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## @XXX(stdmatt): remove the hard coded path...
GEN_BIN_PATH="/Users/stdmatt/Documents/Projects/3rd_party/webfont-generator/bin/generate-webfonts";
OUTPUT_PATH="./__output";

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## Clean the output directory.
rm -rf "$OUTPUT_PATH";
mkdir  "$OUTPUT_PATH";

## Iterate for all the .ttf fonts that we have
## and create the woff and css for them.
find . -iname "*.ttf" > list.txt
while read FULLPATH; do
    FILENAME=$(basename "$FULLPATH");
    FILENAME_NO_EXT=$(basename "$(basename "$FULLPATH" ".ttf")" ".TTF");
    CSS_FILENAME="${OUTPUT_PATH}/${FILENAME_NO_EXT}.css";
    FORMAT="woff2";

    echo "Filename             : ($FILENAME)";
    echo "Filename no extension: ($FILENAME_NO_EXT)";
    echo "CSS filename         : ($CSS_FILENAME)";

    $GEN_BIN_PATH "$FULLPATH"    \
        -o       "$OUTPUT_PATH"  \
        --css    "$CSS_FILENAME" \
        --format "$FORMAT"       \
        --prefix ""
    echo "";
done < list.txt

## Clean the generated .ttf that we don't need...
find "$OUTPUT_PATH" -iname "*.ttf" -delete;
rm -rf ./list.txt
